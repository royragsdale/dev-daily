---
title: "Starting Point"
author: ""
type: ""
date: 2020-01-11T12:51:46-05:00
subtitle: ""
image: ""
tags: [chromeos]
---

As mentioned in my [new year's plan](../2020-01-04#motivation) a motivation for
my current project is having an End-of-life (EOL) Chromebook and wanting to
bring it up to date.


## The Hardware

**Acer C720**, as pulled from the helpful [Developer Information for Chrome OS Devices][dd]

<!--more-->

[dd]:https://sites.google.com/a/chromium.org/dev/chromium-os/developer-information-for-chrome-os-devices

Field                 | Data
----------------------|-------------------------------
Release               | September 12, 2013
OEM                   | Acer
Model                 | Acer C720/C70P/C740 Chromebook
Code name             | Peppy & Pepto
Board name(s)         | peppy
Base board            | slippy
User ABI              | x86_64
Kernel                | 3.8
Kernel ABI            | x86_64
Platform              | Haswell
Form Factor           | Chromebook
First Release         | R30
EOL/AUE               | R76
USB Gadget            | No
Closed Case Debugging | No


Prior to power washing the device it showed:

- Version: `76.0.3809.136 (Official Build) (64-bit)`
- Platform: `12239.92.0 (Official Build)`
- Channel: `stable-channel peppy`
- Firmware: `Google_Peppy.4389.117.0`
- Build Date: `Tuesday, August 27, 2019`
- Kernel: `Linux localhost 3.8.11 #1 SMP Tue Aug 27 16:53:30 PDT 2019 x86_64 Intel(R) Celeron(R) 2955U @ 1.40GHz GenuineIntel GNU/Linux`

![About Chrome OS](../../img/c720_about_chromeOS.png)
![Detailed Build Information](../../img/c720_detailed_build_info.png)


### Firmware Aside

Interestingly while power washing I was provided an option to "Update firmware for added security":

![Reset Chrome Device](../../img/reset_chrome_device.jpg)
![Update Chromebook Security](../../img/update_chromebook_security.jpg)

Even more interesting was this did not result in a higher firmware version.
After the power wash and logging in as a guest it still reports
`Google_Peppy.4389.117.0`. To cap it all off a second power wash did not prompt
for a firmware update???

While looking for more information on this I found the [Trusted Platform Module
firmware vulnerability: technical documentation][ff] (fortunately my machine was
properly patched. Helpfully though this referenced `chrome://system` as a place
to get detailed firmware information. It has a ton of great information.

[ff]:https://www.chromium.org/chromium-os/tpm_firmware_update

For example the `ec_info`:

```
vendor               | ti
name                 | Unknown-10de
fw_version           | peppy_v1.5.133-9078375
```

This refers to the [embedded controller][ec], which how cool is it that all this
information is publicly available.

[ec]:https://chromium.googlesource.com/chromiumos/platform/ec/+/master/README.md

Similarly the `tpm_version`:

```
tpm_family 312e3200
spec_level 0000000200000003
vendor 49465800
tpm_model ffffffff
firmware_version 0000000000000422
vendor_specific 042203f20274706d3338ffffff
version_fingerprint 487001745 1d070e91
```

Note version `0000000000000422 - 4.34` is fixed. Yay.

As a final note the `dmesg` output had a few interesting lines. The first of
which is perhaps the aforementioned firmware update (though oddly it seem to get
applied every boot)?

```
[   12.150597] hda-intel 0000:00:1b.0: Applying patch firmware 'peppy_alsa.fw'
```

The other of which, is probably an interesting rabbit hole:

```
[    0.097482] chromeos: tpm attack callbacks: -19
```

Some quick references are probably [Issue 473051: Unexpected error is seen while
recovering on peppy and falco][ii] and the linked [commit][com]. As well as the
remark

> CHROMIUM: ignore errors when registering tpm attack callbacks.
> 
> This feature is only used on some platforms and only for
> logging purposes, so it is safe to let it not work.


[ii]:https://bugs.chromium.org/p/chromium/issues/detail?id=473051
[com]:https://chromium-review.googlesource.com/c/chromiumos/third_party/kernel/+/262451/


Enough on the firmware rabbit hole, but one final tidbit I found interesting is
that even to get to guest mode seemed to required network connectivity and
accepting some Terms of Service. A quick search did not immediately reveal a way
around this. If would be interesting to log this connection and see what, if
any, traffic is actually sent.

## On to Developer Mode


Consulting the [Debug Button Shortcuts][dbs] has all kinds of interesting key
combinations. The specific one we want is Recovery Mode (`Esc + Refresh and
press Power`) then Developer Mode (`Ctrl + D`).

[dbs]:https://chromium.googlesource.com/chromiumos/docs/+/master/debug_buttons.md

It really is that easy.

## Meta

I have power washed chromebooks and worked in developer mode a number of times
previously, but this all goes to show that you can always dig deeper and learn
more, even doing tasks that seem straight forward.


## Next

I've actually gone ahead and build a chromiumos image, but I'm going to leave
describing that (and testing it to another session).
